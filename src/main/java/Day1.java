import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day1 implements Day {

    private final int day;
    private final ArrayList<String> fileData;
    private List<Integer> calories;

    public Day1(String fileName) {
        fileData = new ArrayList<>();
        calories = new ArrayList<>();
        day = 1;
        this.readFile(fileName);
    }

    @Override
    public int getDayNum() {
        return day;
    }

    public void readFile(String fileName) {
        try (Scanner scanner = new Scanner(new File(fileName))) {
            while (scanner.hasNextLine()) {
                fileData.add(scanner.nextLine());
            }

            int[] indexes = Stream
                    .of(IntStream.of(-1), IntStream.range(0, fileData.size()).filter(i -> fileData.get(i).isBlank()),
                            IntStream.of(fileData.size()))
                    .flatMapToInt(s -> s).toArray();
            List<List<String>> subSets = IntStream.range(0, indexes.length - 1)
                    .mapToObj(i -> fileData.subList(indexes[i] + 1, indexes[i + 1])).toList();

            calories = subSets.stream().map(s -> s.stream().mapToInt(Integer::valueOf).sum())
                    .collect(Collectors.toList());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int part1() {
        return calories.stream().max(Integer::compareTo).orElse(-1);
    }

    @Override
    public int part2() {
        Collections.sort(calories);
        return new ArrayList<>(calories.subList(calories.size() - 3, calories.size())).stream().reduce(0,
                Integer::sum);
    }
}
