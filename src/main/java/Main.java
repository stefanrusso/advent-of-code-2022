import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Day> days = new ArrayList<>();

        days.add(new Day1("day1.txt"));

        days.add(new Day2("day2.txt"));

        days.add(new Day3("day3.txt"));

        days.add(new Day4("day4.txt"));


        for (Day day : days) {
            System.out.println("Day: " + day.getDayNum() + " Part 1: " + day.part1() + " Part 2: " + day.part2());
        }
    }
}