import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Day2 implements Day {

    private final int day;
    private final ArrayList<String> fileData;

    public Day2(String fileName) {
        fileData = new ArrayList<>();
        day = 2;
        this.readFile(fileName);
    }

    public void readFile(String fileName) {
        try (Scanner scanner = new Scanner(new File(fileName))) {
            while (scanner.hasNextLine()) {
                fileData.add(scanner.nextLine());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getPoints(String line) {
        HashMap<String, Integer> values = new HashMap<>();
        values.put("A", 1);
        values.put("B", 2);
        values.put("C", 3);
        values.put("X", 1);
        values.put("Y", 2);
        values.put("Z", 3);

        String[] hand = line.split(" ");
        String opponent = hand[0];
        String you = hand[1];

        int winner = (values.get(opponent) - values.get(you) + 2) % 3;
        if (winner == 0) {
            // opponent wins, return 0 points
            return values.get(you);
        } else if (winner == 1) {
            // you win, return 6
            return 6 + values.get(you);
        } else {
            // tie, return 3
            return 3 + values.get(you);
        }
    }

    public String getHand(String line) {
        HashMap<String, Integer> values = new HashMap<>();
        values.put("A", 1);
        values.put("B", 2);
        values.put("C", 3);
        values.put("X", 1);
        values.put("Y", 2);
        values.put("Z", 3);
        // winner = (opponent - you + 2) % 3
        String[] hand = line.split(" ");
        String opponent = hand[0];
        String you = hand[1];

        HashMap<String, Integer> wld = new HashMap<>();
        wld.put("X", 0);
        wld.put("Y", 2);
        wld.put("Z", 1);

        int card = (wld.get(you) + values.get(opponent) + (wld.get(you) + 2) % 3) % 3;
        String myCard;
        if (card == 0)
            myCard = "Z";
        else if (card == 1)
            myCard = "X";
        else
            myCard = "Y";

        return opponent + " " + myCard;
    }

    @Override
    public int part1() {
        return fileData.stream()
                .mapToInt(this::getPoints)
                .sum();
    }

    @Override
    public int part2() {
        return fileData.stream()
                .map(this::getHand)
                .mapToInt(this::getPoints)
                .sum();
    }

    @Override
    public int getDayNum() {
        return day;
    }
}
