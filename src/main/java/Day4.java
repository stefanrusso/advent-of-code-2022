import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Day4 implements Day {
    private int day;
    private String filename;
    private ArrayList<String> fileData;

    public Day4(String filename) {
        this.fileData = new ArrayList<>();
        this.readFile(filename);
        day = 4;
    }

    public void readFile(String fileName) {
        try (Scanner scanner = new Scanner(new File(fileName))) {
            while (scanner.hasNextLine()) {
                fileData.add(scanner.nextLine());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean p1contains(String line) {
        String[] snums = line.split("[,-]");
        int[] nums = Arrays.stream(snums)
                .mapToInt(n -> Integer.valueOf(n))
                .toArray();
        return (nums[0] <= nums[2] && nums[1] >= nums[3]) || (nums[0] >= nums[2] && nums[1] <= nums[3]);
    }

    @Override
    public int part1() {
        return Math.toIntExact(fileData.stream()
//                .map(s -> s.split("[,-]"))
//                .filter(s -> Math.max(Integer.parseInt(s[0]), Integer.parseInt(s[2])) <= Math.min(Integer.parseInt(s[1]), Integer.parseInt(s[3])))
                .filter(this::p1contains)
                .count());
    }

    @Override
    public int part2() {
        return Math.toIntExact(fileData.stream()
                .map(s -> s.split("[,-]"))
                .filter(s -> Math.max(Integer.parseInt(s[0]), Integer.parseInt(s[2])) <= Math.min(Integer.parseInt(s[1]), Integer.parseInt(s[3])))
                .count());
    }

    @Override
    public int getDayNum() {
        return day;
    }
}
