public interface Day {
    int part1();

    int part2();

    int getDayNum();
}
