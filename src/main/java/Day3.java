import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Day3 implements Day {
    private final int day;
    private final ArrayList<String> fileData;


    public Day3(String fileName) {
        fileData = new ArrayList<>();
        day = 2;
        this.readFile(fileName);
    }

    public void readFile(String fileName) {
        try (Scanner scanner = new Scanner(new File(fileName))) {
            while (scanner.hasNextLine()) {
                fileData.add(scanner.nextLine());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getPriority(char c) {
        return "-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(c);
    }

    @Override
    public int part1() {
        int part1 = 0;
        for (String fileDatum : fileData) {
            String firstHalf = fileDatum.substring(0, fileDatum.length() / 2);
            String secondHalf = fileDatum.substring(fileDatum.length() / 2);

            for (char c : secondHalf.toCharArray()) {
                if (firstHalf.indexOf(c) >= 0) {
                    part1 += getPriority(c);
                    break;
                }
            }

        }
        return part1;
    }

    @Override
    public int part2() {
        int part2 = 0;
        for (int i = 0; i < fileData.size(); i++) {
            if ((i + 1) % 3 == 0) {
                String line1 = fileData.get(i - 2);
                String line2 = fileData.get(i - 1);
                String line3 = fileData.get(i);

                for (char c : line1.toCharArray()) {
                    if (line2.indexOf(c) >= 0 && line3.indexOf(c) >= 0) {
                        part2 += getPriority(c);
                        break;
                    }
                }
            }
        }
        return part2;
    }

    @Override
    public int getDayNum() {
        return day;
    }
}
