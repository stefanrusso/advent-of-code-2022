import static org.junit.jupiter.api.Assertions.assertEquals;

class Day1Test {

    @org.junit.jupiter.api.Test
    void part1() {
        Day1 testD1 = new Day1("day1.txt");
        assertEquals(70374, testD1.part1());
    }

    @org.junit.jupiter.api.Test
    void part2() {
        Day1 testD1 = new Day1("day1.txt");
        assertEquals(204610, testD1.part2());
    }
}