import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day3Test {

    @Test
    void part1() {
        Day3 d3 = new Day3("day3.txt");
        assertEquals(7727, d3.part1());
    }

    @Test
    void part2() {
        Day3 d3 = new Day3("day3.txt");
        assertEquals(2609, d3.part2());
    }
}