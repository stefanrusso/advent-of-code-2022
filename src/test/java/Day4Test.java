import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Day4Test {

    @Test
    void part1() {
        Day4 d4 = new Day4("day4.txt");
        assertEquals(485, d4.part1());
    }

    @Test
    void part2() {
        Day4 d4 = new Day4("day4.txt");
        assertEquals(857, d4.part2());
    }
}