import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day2Test {

    @Test
    void part1() {
        Day2 d2 = new Day2("day2.txt");
        assertEquals(10718, d2.part1());
    }

    @Test
    void part2() {
        Day2 d2 = new Day2("day2.txt");
        assertEquals(14652, d2.part2());
    }
}